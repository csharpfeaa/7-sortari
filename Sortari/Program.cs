﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortari
{
    /**
     * Implementarea algoritmilor de sortare: Bubble Sort si Insert Sort
     */
    class Program
    {
        static void Main(string[] args)
        {
            int[] numere = {12, 11, 13, 5, 6};
            Console.Write(numere);
            
            BubbleSort(numere);
            //BubbleSort2(numere);
            //InsertSort(numere);

            Console.ReadKey();
        }

        static void BubbleSort(int[] v)
        {
            bool schimbare = true;

            while (schimbare)
            {
                schimbare = false;

                for (int i = 0; i < v.Length - 1; i++)
                {
                    if (v[i] > v[i + 1])
                    {
                        int temp = v[i + 1];
                        v[i + 1] = v[i];
                        v[i] = temp;

                        schimbare = true;
                    }
                }

                AfiseazaVector(v);

                Console.WriteLine();
            }
        }
        
        static void BubbleSort2(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                for (int j = 0; j < v.Length - i - 1; j++)
                {
                    if (v[j] > v[j + 1])
                    {
                        int temp = v[j];
                        v[j] = v[j + 1];
                        v[j + 1] = temp;
                    }
                }
                
                AfiseazaVector(v);
            }
            
            
        }

        static void InsertSort(int[] v)
        {
            for (int i = 1; i < v.Length; i++)
            {
                int j = i;

                while (j > 0 && v[j - 1] > v[j])
                {
                    int tmp = v[j];
                    v[j] = v[j - 1];
                    v[j - 1] = tmp;

                    j--;
                }
            }

            AfiseazaVector(v);
        }

        static void AfiseazaVector(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                Console.Write(v[i]);

                if (i < v.Length - 1)
                    Console.Write(", ");
            }

            Console.WriteLine();
        }
    }
}